//
//  FirstViewController.swift
//  MyGuessingApp
//
//  Created by Vagicharla,Venkat Raghava Sampath on 2/28/19.
//  Copyright © 2019 Vagicharla,Venkat Raghava Sampath. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    
    @IBOutlet weak var CorrectGuessLBL: UILabel!
    @IBAction func AmIRightBTN(_ sender: Any) {
        if let value = Int(GuessTF.text!) {
            let result = Guesser.shared.amIRight(guess: Int(value))
            CorrectGuessLBL.text = result.rawValue
            if result == Result.correct{
                CorrectGuessLBL.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                displayMessage()
                Guesser.shared.createNewProblem()
            }else if result.rawValue.contains("Too") {
                CorrectGuessLBL.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Guesser.shared.createNewProblem()
    }
    @IBOutlet weak var GuessTF: UITextField!
    
    @IBAction func CreateNewProblemBtn(_ sender: Any) {
        CorrectGuessLBL.text = ""
        GuessTF.text = ""
        Guesser.shared.createNewProblem()
    }
    
    
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",
                                      message: "You got it in \(Guesser.shared.numAttempts) attempts",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

